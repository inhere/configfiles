# sublime 3 packages and settings backup

## sublime常用包

- 使用前得保证先安装了`Package Control`

### 使用方法

在 sublime 中 `Perferences -> Browse Packages` , 打开本地的 `Package\ Control.sublime-settings`
查看 [installed packages](./Package\ Control.sublime-settings) 上面的包名称，根据需要复制到本地文件中
ok，现在重启sublime, 他就会自动安装还没有的包 :)

## 个人设置

一些不错的sublime设置。

在 sublime 中 `Perferences -> Settings User` , 打开当前的用户配置。
将 [settings](./Perferences.sublime-settings) 中的内容复制并覆盖到本地配置。

也可以根据需要拷贝配置项。

