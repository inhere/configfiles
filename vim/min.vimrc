" 最小化 -- 无插件
"

syntax on
filetype on                                           "启用文件类型侦测

set hlsearch                                          "高亮搜索
set incsearch                                         "在输入要搜索的文字时，实时匹配
set ignorecase                                        "搜索模式里忽略大小写
set smartcase                                         "如果搜索模式包含大写字符，不使用 'ignorecase' 选项，只有在输入搜索模式并且打开 'ignorecase' 选项时才会使用

set t_Co=256                                          " 在终端启用256色
set nocompatible                                      " 禁用 Vi 兼容模式

" 设置退格键可用
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

" 注：使用utf-8格式后，软件与程序源码、文件路径不能有中文，否则报错
set encoding=utf-8                                    "设置gvim内部编码，默认不更改
set fileencoding=utf-8                                "设置当前文件编码，可以更改，如：gbk（同cp936）
set fileencodings=ucs-bom,utf-8,gbk,cp936,latin-1     "设置支持打开的文件的编码

" 文件格式，默认 ffs=dos,unix
set fileformat=unix                                   "设置新（当前）文件的<EOL>格式，可以更改，如：dos（windows系统常用）
set fileformats=unix,dos,mac                          "给出文件的<EOL>格式类型

set smartindent                                       "启用智能对齐方式
set expandtab                                         "将Tab键转换为空格
set tabstop=4                                         "设置Tab键的宽度，可以更改，如：宽度为2
set shiftwidth=4                                      "换行时自动缩进宽度，可更改（宽度同tabstop）
set smarttab                                          "指定按一次backspace就删除shiftwidth宽度
set autoread                                          " 当文件在外部被修改，自动更新该文件

autocmd! bufwritepost _vimrc source %               " vimrc文件修改之后自动加载, windows
autocmd! bufwritepost .vimrc source %               " vimrc文件修改之后自动加载, linux

" 常规模式下用空格键来开关光标行所在折叠（注：zR 展开所有折叠，zM 关闭所有折叠）
nnoremap <space> @=((foldclosed(line('.')) < 0) ? 'zc' : 'zo')<CR>

" save
cmap w!! w !sudo tee >/dev/null %

" 常规模式下输入 cS 清除行尾空格
nmap cS :%s/\s\+$//g<CR>:noh<CR>

" 常规模式下输入 cM 清除行尾 ^M 符号
nmap cM :%s/\r$//g<CR>:noh<CR>


imap <c-k> <Up>         " Ctrl + K 插入模式下光标向上移动
imap <c-j> <Down>       " Ctrl + J 插入模式下光标向下移动
imap <c-h> <Left>       " Ctrl + H 插入模式下光标向左移动
imap <c-l> <Right>      " Ctrl + L 插入模式下光标向右移动

" -----------------------------------------------------------------------------
"  < 界面配置 >
" -----------------------------------------------------------------------------
set number                                            "显示行号
set laststatus=2                                      "启用状态栏信息
set cmdheight=2                                       "设置命令行的高度为2，默认为1
set cursorline                                        "突出显示当前行
set cursorcolumn " 突出显示当前列

set ruler                   " 显示当前的行号列号
set showcmd                 " 在状态栏显示正在输入的命令

colorscheme desert

set guifont=Consolas:h12
set nowrap                                            "设置不自动换行
set shortmess=atI                                     "去掉欢迎界面
set nobackup                                          "设置无备份文件
set noswapfile                                        "关闭交换文件

" 设置 退出vim后，内容显示在终端屏幕, 可以用于查看和复制, 不需要可以去掉
" 好处：误删什么的，如果以前屏幕打开，可以找回
set t_ti= t_te=
set scrolloff=7             " 在上下移动光标时，光标的上方或下方至少会保留显示的行数

" 00x增减数字时使用十进制
set nrformats=

" 相对行号: 行号变成相对，可以用 nj/nk 进行跳转
set relativenumber number
au FocusLost * :set norelativenumber number
au FocusGained * :set relativenumber
" 插入模式下用绝对行号, 普通模式下用相对
autocmd InsertEnter * :set norelativenumber number
autocmd InsertLeave * :set relativenumber
function! NumberToggle()
    if(&relativenumber == 1)
       set norelativenumber number
    else
       set relativenumber
    endif
endfunc
nnoremap <C-n> :call NumberToggle()<cr>

" 在不使用 MiniBufExplorer 插件时也可用<C-k,j,h,l>切换到上下左右的窗口中去
noremap <c-k> <c-w>k
noremap <c-j> <c-w>j
noremap <c-h> <c-w>h
noremap <c-l> <c-w>l

" 自动切换目录为当前编辑文件所在目录 close at 2015.12.18
au BufRead,BufNewFile,BufEnter * cd %:p:h
let mapleader = ","