# vim config
-------------------

## How to use

> `git` is required 

`full.vimrc` is a full config.

### At Linux

```
$ mv ~/.vim ~/.vim_bak     // backup old vim files.
$ git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
$ curl http://git.oschina.net/inhere/vim-pkg-config/raw/master/full.vimrc -o ~/.vimrc
```

OK.

### At Windows(gVim)

go to `gVim` dir

```
$ git clone https://github.com/VundleVim/Vundle.vim.git vimfiles/bundle/Vundle.vim
$ curl http://git.oschina.net/inhere/vim-pkg-config/raw/master/full.vimrc -o _vimrc
```

OK.

## simple config

简洁版配置

## min config

`min.vimrc` - 最小化的配置，无插件。适合在服务器上使用

```
curl http://git.oschina.net/inhere/vim-pkg-config/raw/master/min.vimrc -o ~/.vimrc
```
